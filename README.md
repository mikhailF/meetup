# Overview
Contains content for SPB DotNet Meetup

## Useful links

### MSBuild

- [MSBuildSdks](https://github.com/microsoft/MSBuildSdks)
- [Customize your build](https://docs.microsoft.com/en-us/visualstudio/msbuild/customize-your-build?view=vs-2022)

### Static analyzers

- [StyleCop](https://github.com/DotNetAnalyzers/StyleCopAnalyzers)
- [ClrHeapAllocationAnalyzer](https://github.com/microsoft/RoslynClrHeapAllocationAnalyzer)

### Other articles

- [Setting up a .NET Project in 2021](https://www.ethan-shea.com/posts/setting-up-dotnet-2021)
- [Building .NET projects using the Microsoft.Build.Traversal SDK](https://www.meziantou.net/building-dotnet-projects-using-the-microsoft-build-traversal-sdk.htm)
- [MSBuild Tutorial](https://blog.elmah.io/msbuild-tutorial/)

